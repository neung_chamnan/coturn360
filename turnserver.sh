#!/bin/bash

if [ -z $SKIP_AUTO_IP ] && [ -z $EXTERNAL_IP ]
then
    if [ ! -z USE_IPV4 ]
    then
        EXTERNAL_IP=`curl -4 icanhazip.com 2> /dev/null`
    else
        EXTERNAL_IP=`curl icanhazip.com 2> /dev/null`
    fi
fi

if [ -z $PORT ]
then
    PORT=3478
fi


if [ -z $SKIP_AUTO_IP ]
then
    echo external-ip=$EXTERNAL_IP >> /etc/coturn.conf
fi
echo listening-port=$PORT >> /etc/coturn.conf

if [ ! -z $LISTEN_ON_PUBLIC_IP ]
then
    echo listening-ip=$EXTERNAL_IP >> /etc/coturn.conf
fi

exec /usr/bin/turnserver -c /etc/coturn.conf --log-file stdout
# See all options at https://github.com/coturn/coturn/wiki/turnserver